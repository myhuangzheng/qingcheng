package com.qingcheng.service.goods;

import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.goods.Brand;

import java.util.List;
import java.util.Map;

public interface BrandService {
    //查询所有产品信息
    public List<Brand> findAll();
    //返回查询分页数据
    public PageResult<Brand> findPage(int page ,int size);
    //根据条件查询数据
    public List<Brand> findList(Map<String,Object> searchMap);
    //根据条件查询分页信息
    public PageResult<Brand> findPage(Map<String,Object> searchMap,int page ,int size);
    //根据id查询产品名称
    public Brand findById(Integer id);
    //新增产品信息
    public void add(Brand brand);
    //修改产品信息
    public void update(Brand brand);
    //删除指定产品信息
    public void delete(Integer id);
}
