package com.qingcheng.controller.goods;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.Result;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.goods.Brand;
import com.qingcheng.service.goods.BrandService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Reference
    private BrandService brandService;

    //查询所有品牌信息
    @RequestMapping("/findAll")
    public List<Brand> findAll() {
        List<Brand> brands = brandService.findAll();
        return brands;
    }

    //查询分页数据
    @GetMapping("/findPage")
    public PageResult<Brand> findPage(int page, int size) {
        return brandService.findPage(page, size);
    }

    //根据条件进行查询
    @PostMapping("/findList")
    public List<Brand> findList(@RequestBody Map searchMap) {
        return brandService.findList(searchMap);
    }

    //查询数据进行分页展示数据
    @PostMapping("/findPage")
    public PageResult<Brand> findPage(@RequestBody Map searchMap, int page, int size) {
        return brandService.findPage(searchMap, page, size);
    }

    //根据id进行查询参数名称
    @GetMapping("/findById")
    public Brand findById(Integer id) {
        return brandService.findById(id);
    }

    //增加产品信息
    @PostMapping("/add")
    public Result add(@RequestBody Brand brand) {
//        int x =1/0;
        brandService.add(brand);
        return new Result();
    }

    //修改产品数据
    @PostMapping("/update")
    public Result update(@RequestBody Brand brand) {
        brandService.update(brand);
        return new Result();
    }
    //删除产品数据
    @GetMapping("/delete")
    public Result delete(Integer id) {
        brandService.delete(id);
        return new Result();
    }
}
