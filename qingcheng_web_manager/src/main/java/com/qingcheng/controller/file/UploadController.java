package com.qingcheng.controller.file;

import com.aliyun.oss.OSSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传类
 */
@RestController
@RequestMapping("/upload")
public class UploadController {
    @Autowired
    private HttpServletRequest request;

    @PostMapping("/native")
    public String nativeUpload(@RequestParam("file") MultipartFile file){
//        获取本地文件夹的绝对路径,
        String path = request.getSession().getServletContext().getRealPath("img");
        //获取目录下的文件名
        String filePath=path+"/"+file.getOriginalFilename();
        //获取文件对象
        File desFile=new File(filePath);
//        判断文件目录是否存在
        if(!desFile.getParentFile().exists()){
            //如果目录不存在.就创建一个文件夹
            desFile.mkdirs();
        }
        try {
            file.transferTo(desFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //返回的是一个目录,文件的路径
        return "http://localhost:9101/img/"+file.getOriginalFilename();
    }

    @Autowired
    private OSSClient ossClient;

    @PostMapping("/oss")
//    传入存储非文件目录名称
    public String ossUpload(@RequestParam("file") MultipartFile file,String folder){
//      空间名称
        String bucketName="qingchengdianshangds";
//      获取原始文件名 使文件存储不重名,防止文件覆盖
        String filename =folder+ UUID.randomUUID()+ file.getOriginalFilename();
//      输入流
        try {
            ossClient.putObject(bucketName,filename,file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "https://"+bucketName+".oss-cn-beijing.aliyuncs.com/"+filename;
    }
}
