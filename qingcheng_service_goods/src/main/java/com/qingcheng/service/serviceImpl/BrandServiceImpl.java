package com.qingcheng.service.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.BrandMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.goods.Brand;
import com.qingcheng.service.goods.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

//服务层
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private BrandMapper brandMapper;

    //查询所有brand表中数据
    @Override
    public List<Brand> findAll() {
      return  brandMapper.selectAll();
    }

    //查询所有数据的条数,分页查询
    @Override
    public PageResult<Brand> findPage(int page, int size) {
        /**
         *  分页查询工具,两者紧密相连.中间不得插入参数
         *  传入用户传来的参数
         *  查询所有数据,进行分页展示
         */
       PageHelper.startPage(page,size);
       //配置插件提供的Page类,
       Page<Brand> pageResult= (Page<Brand>) brandMapper.selectAll();
        //传入参数交给mapper接口处理查询数据
        return new PageResult<>(pageResult.getTotal(),pageResult.getResult());
    }

    //根据条件查询
    @Override
    public List<Brand> findList(Map<String, Object> searchMap) {
        //调用抽取方法
        //Example 查询条件的封装对象,selectByExample根据条件查询
        Example example = createExample(searchMap);
        return brandMapper.selectByExample(example);
    }

    //根据条件进行查询数据,并分页展示
    @Override
    public PageResult<Brand> findPage(Map<String, Object> searchMap, int page, int size) {

        //进行分页
        PageHelper.startPage(page,size);
        //根据条件进行查询数据.查出所有符合条件的数据
        Example example = createExample(searchMap);
        Page<Brand> pageResult= (Page<Brand>) brandMapper.selectByExample(example);

        return new PageResult<>(pageResult.getTotal(),pageResult.getResult());
    }

    //根据id进行查询数据名称
    @Override
    public Brand findById(Integer id) {
     return brandMapper.selectByPrimaryKey(id);
    }

    //增加用户信息
    @Override
    public void add(Brand brand) {
        brandMapper.insert(brand);
    }

    //修改用户信息
    @Override
    public void update(Brand brand) {
        brandMapper.updateByPrimaryKeySelective(brand);
    }

    //根据id进行产品删除
    @Override
    public void delete(Integer id) {
        brandMapper.deleteByPrimaryKey(id);
    }


    //抽取模糊查询和匹配查询
    private Example createExample(Map<String, Object> searchMap){
        //查询的类的class
        Example exampele=new Example(Brand.class);
        Example.Criteria criteria = exampele.createCriteria();
        //判断对象不为空在进入查询防止报错
        if(searchMap!=null){
            //再次判断对象根据键获取值不为空时，进行查询数据
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                //andLike   模糊查询
                criteria.andLike("name","%"+ (String) searchMap.get("name")+"%");
            }
            if(searchMap.get("letter")!=null&& !"".equals(searchMap.get("letter"))){
                //andLike  匹配查询
                criteria.andEqualTo("letter",(String) searchMap.get("letter"));
            }
        }
        return exampele;
    }
}
